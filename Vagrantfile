# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

    config.vbguest.auto_update = true

    config.vm.define :gitlab do |gitlab_config|
      gitlab_config.vm.box = "debian/jessie64"
      gitlab_config.vm.hostname = "gitlab"
      gitlab_config.vm.network "private_network", ip: "192.168.50.201"
      gitlab_config.vm.synced_folder ".", "/vagrant", disabled: true
      gitlab_config.vm.provider :virtualbox do |vb|
        vb.customize ["modifyvm", :id, "--memory", "1536"]
      end
      gitlab_config.ssh.forward_agent = true
      gitlab_config.ssh.insert_key = false
      gitlab_config.ssh.private_key_path = ["~/.vagrant.d/insecure_private_key" ]

      gitlab_config.vm.provision "ansible" do |ansible|
        ansible.playbook = "playbook.yml"
        ansible.inventory_path = "ansible_hosts"
        ansible.limit = "gitlab"
      end

    end

    config.vm.define :jenkins, autostart: false do |jenkins_config|
      jenkins_config.vm.box = "debian/jessie64"
      jenkins_config.vm.hostname = "jenkins"
      jenkins_config.vm.network "private_network", ip: "192.168.50.202"
      jenkins_config.vm.synced_folder ".", "/vagrant", disabled: true
      jenkins_config.vm.provider :virtualbox do |vb|
        vb.customize ["modifyvm", :id, "--memory", "1024"]
      end
      jenkins_config.ssh.forward_agent = true
      jenkins_config.ssh.insert_key = false
      jenkins_config.ssh.private_key_path = ["~/.vagrant.d/insecure_private_key" ]

      jenkins_config.vm.provision "ansible" do |ansible|
        ansible.playbook = "playbook.yml"
        ansible.inventory_path = "ansible_hosts"
        ansible.ask_vault_pass = true
        ansible.limit = "jenkins"
      end

    end

    config.vm.define :postgresql, autostart: false do |postgresql_config|
      postgresql_config.vm.box = "debian/jessie64"
      postgresql_config.vm.hostname = "postgresql"
      postgresql_config.vm.network "private_network", ip: "192.168.50.203"
      postgresql_config.vm.synced_folder ".", "/vagrant", disabled: true
      postgresql_config.vm.provider :virtualbox do |vb|
        vb.customize ["modifyvm", :id, "--memory", "1024"]
      end
      postgresql_config.ssh.forward_agent = true
      postgresql_config.ssh.insert_key = false
      postgresql_config.ssh.private_key_path = ["~/.vagrant.d/insecure_private_key" ]

      postgresql_config.vm.provision "ansible" do |ansible|
        ansible.playbook = "playbook.yml"
        ansible.inventory_path = "ansible_hosts"
        ansible.limit = "postgresql"
      end

    end

    config.vm.define :python_service, autostart: false do |python_service_config|
      python_service_config.vm.box = "debian/jessie64"
      python_service_config.vm.hostname = "python-service"
      python_service_config.vm.network "private_network", ip: "192.168.50.204"
      python_service_config.vm.synced_folder ".", "/vagrant", disabled: true
      python_service_config.vm.provider :virtualbox do |vb|
        vb.customize ["modifyvm", :id, "--memory", "1024"]
      end
      python_service_config.ssh.forward_agent = true
      python_service_config.ssh.insert_key = false
      python_service_config.ssh.private_key_path = ["~/.vagrant.d/insecure_private_key" ]

      python_service_config.vm.provision "ansible" do |ansible|
        ansible.playbook = "playbook.yml"
        ansible.inventory_path = "ansible_hosts"
        ansible.limit = "python-service"
      end

    end

end
