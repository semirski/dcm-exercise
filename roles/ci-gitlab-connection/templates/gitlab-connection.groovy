#!groovy

import jenkins.model.*
import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.common.*
import com.cloudbees.plugins.credentials.domains.*
import com.dabsquared.gitlabjenkins.connection.*
import jenkins.model.Jenkins

global_domain = Domain.global()
credentials_store = Jenkins.instance.getExtensionList(
            'com.cloudbees.plugins.credentials.SystemCredentialsProvider'
          )[0].getStore()
c = hudson.util.Secret.fromString("{{gitlab_token_id}}")
credentials = new GitLabApiTokenImpl(com.cloudbees.plugins.credentials.CredentialsScope.valueOf('GLOBAL'),
                                     java.util.UUID.randomUUID().toString(),
                                     '',
                                     c)
credentials_store.addCredentials(global_domain, credentials)
gitlab_token_id = credentials.getId()

GitLabConnectionConfig descriptor = (GitLabConnectionConfig) Jenkins.getInstance().getDescriptor(GitLabConnectionConfig.class)
GitLabConnection gitLabConnection = new GitLabConnection('{{gitlab_connection_name}}',
                                        '{{gitlab_url}}',
                                        "${gitlab_token_id}",
                                        false,
                                        10,
                                        10)
descriptor.getConnections().clear()
descriptor.addConnection(gitLabConnection)
descriptor.save()
