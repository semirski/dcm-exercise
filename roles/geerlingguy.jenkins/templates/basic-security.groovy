#!groovy
import hudson.security.*
import jenkins.model.*
import jenkins.security.*
import hudson.model.User;
import jenkins.security.ApiTokenProperty;

def instance = Jenkins.getInstance()

println "--> Checking if security has been set already"

if (!instance.isUseSecurity()) {
    println "--> creating local user 'admin'"

    def hudsonRealm = new HudsonPrivateSecurityRealm(false)
    hudsonRealm.createAccount('{{ jenkins_admin_username }}', '{{ jenkins_admin_password }}')
    instance.setSecurityRealm(hudsonRealm)

    def strategy = new FullControlOnceLoggedInAuthorizationStrategy()
    instance.setAuthorizationStrategy(strategy)
    instance.save()
}

def file2 = new File('/tmp/jjb.txt')
User u = User.get('{{ jenkins_admin_username }}')
ApiTokenProperty t = u.getProperty(ApiTokenProperty.class)
def token = t.getApiTokenInsecure()
file2 << "${token}"
